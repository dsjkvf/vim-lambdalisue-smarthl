vim-lambdalisue-smarthl
=======================

## About

This is a fork of [vim-smarthl](https://github.com/lambdalisue/smarthl.vim) plugin by [Alisue](https://github.com/lambdalisue/) with some features added. However, I strongly encourage you to use the original plugin, and naturally, all the credits go to the original author unless stated otherwise.

## Changes

  - highlights are cleared on CursorMoved, InsertEnter, WinLeave -- and FocusLost events;
  - ~~current and total numbers of matches are displayed;~~ (not anymore; now, this information is managed by Vim itself, see `:h shortmess`)
  - optional blinking on the current search occurrence (by [Junegunn](https://github.com/junegunn) from his excellent [vim-slash](https://github.com/junegunn/vim-slash/) plugin)
  - `*` and `#` commands now work in visual mode, too (TODO: make these mappings a bit [more robust](https://www.reddit.com/comments/avfmtj//ehf5dw1/)?)
  - `g*` and `g#` are not supported any more.

## Options

First, make sure [hlsearch](http://vimdoc.sourceforge.net/htmldoc/options.html#%27hlsearch%27) option is disabled (which is the default Vim's setting).

Second, if you don't want the current search occurrence to blink, add the following option to your `$VIMRC`:

    let g:smarthl_blinker = 0

Besides that, if you don't want the plugin to overwrite the default mappings, add the following option to your `$VIMRC`:

    let g:smarthl_default_mappings = 0

And then add your own mappings in the following manner:

    nmap / <Plug>(smarthl-/)
    nmap ? <Plug>(smarthl-?)
    nmap * <Plug>(smarthl-*)
    xmap * "zy<Plug>(smarthl-x*)
    nmap # <Plug>(smarthl-#)
    xmap # "zy<Plug>(smarthl-x#)
    nmap n <Plug>(smarthl-n)
    nmap N <Plug>(smarthl-N)
