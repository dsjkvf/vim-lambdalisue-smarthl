" Load
if exists('g:smarthl_loaded')
  finish
endif
let g:smarthl_loaded = 1

" Use blinker
if !exists("g:smarthl_blinker")
    let g:smarthl_blinker = 1
endif

" Set mappings
nnoremap <expr> <Plug>(smarthl-/) smarthl#enter('/')
nnoremap <expr> <Plug>(smarthl-?) smarthl#enter('?')
nnoremap <expr> <Plug>(smarthl-*) smarthl#enter('*')
nnoremap <expr> <Plug>(smarthl-x*) smarthl#enter('x*')
nnoremap <expr> <Plug>(smarthl-#) smarthl#enter('#')
nnoremap <expr> <Plug>(smarthl-x#) smarthl#enter('x#')
nnoremap <expr> <Plug>(smarthl-n) smarthl#enter('n')
nnoremap <expr> <Plug>(smarthl-N) smarthl#enter('N')
" doesn't supported as of now
" nnoremap <expr> <Plug>(smarthl-g*) smarthl#enter('g*')
" nnoremap <expr> <Plug>(smarthl-g#) smarthl#enter('g#')

if get(g:, 'smarthl_default_mappings', 1)
  nmap / <Plug>(smarthl-/)
  nmap ? <Plug>(smarthl-?)
  nmap * <Plug>(smarthl-*)
  xmap * "zy<Plug>(smarthl-x*)
  nmap # <Plug>(smarthl-#)
  xmap # "zy<Plug>(smarthl-x#)
  nmap n <Plug>(smarthl-n)
  nmap N <Plug>(smarthl-N)
" doesn't supported as of now
"   nmap g* <Plug>(smarthl-g*)
"   nmap g# <Plug>(smarthl-g#)
endif
