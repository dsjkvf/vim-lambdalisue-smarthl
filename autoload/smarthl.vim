function! smarthl#enter(expr) abort
    set hlsearch
    augroup smarthl_enter
        autocmd!
        autocmd CursorMoved *
                    \ call smarthl#leave() |
                    \ augroup smarthl_enter |
                    \   autocmd! |
                    \ augroup END
    augroup END

    if a:expr ==# 'n'
        if has('timers') && g:smarthl_blinker == 1
            call smarthl#blinker(3, 50)
        endif
        if !exists('b:the_dir')
            let b:the_dir = 'F'
        endif
"         if b:the_dir == 'F'
"             call smarthl#statusline('F')
"         else
"             call smarthl#statusline('B')
"         endif
"         return 'nzvzz'
        return 'n'
    elseif a:expr ==# 'N'
        if has('timers') && g:smarthl_blinker == 1
            call smarthl#blinker(3, 50)
        endif
        if !exists('b:the_dir')
            let b:the_dir = 'F'
        endif
"         if b:the_dir == 'F'
"             call smarthl#statusline('B')
"         else
"             call smarthl#statusline('F')
"         endif
"         return 'Nzvzz'
        return 'N'
    elseif a:expr == '/'
        let b:the_dir = 'F'
    elseif a:expr == '?'
        let b:the_dir = 'B'
    elseif a:expr == '*'
        let b:the_dir = 'F'
        return ":let @/ = '\\<' . expand('<cword>') . '\\>'\<CR>:call smarthl#leave()\<CR>:echo '/' . @/\<CR>"
    elseif a:expr == '#'
        let b:the_dir = 'B'
        return ":let @/ = '\\<' . expand('<cword>') . '\\>'\<CR>:call smarthl#leave()\<CR>:echo '?' . @/\<CR>"
    elseif a:expr == 'x*'
        let b:the_dir = 'F'
        return ":let @/ = escape(@z,'/\.*$^~[')\<CR>/\<CR>N:call smarthl#leave()\<CR>:echo '/' . @/\<CR>"
    elseif a:expr == 'x#'
        let b:the_dir = 'B'
        return ":let @/ = escape(@z,'/\.*$^~[')\<CR>?\<CR>:call smarthl#leave()\<CR>:echo '?' . @/\<CR>"
    endif
    return a:expr
endfunction

function! smarthl#leave() abort
    augroup smarthl_leave
        autocmd!
        autocmd CursorMoved,InsertEnter,WinLeave,FocusLost *
                    \ set nohlsearch |
                    \ augroup smarthl_leave |
                    \   autocmd! |
                    \ augroup END
    augroup END
    set hlsearch
endfunction

" function! smarthl#statusline(dir) abort
"     let cnt = ''
"     try
"         redir => cnt
"             silent exe '%s/' . @/ . '//gn'
"         redir END
"         let the_total = strpart(cnt, 1, stridx(cnt, "m") - 2)
"         redir => cnt
"             silent exe '.,$s/' . @/ . '//gn'
"         redir END
"         let the_current = strpart(cnt, 1, stridx(cnt, "m") - 2)
" 
"         if a:dir == 'F'
"             let the_curr_fow = the_total - the_current + 2
"             redraw | echo '/'. @/ . ' match ' . the_curr_fow . ' of ' . the_total
"         elseif a:dir == 'B'
"             let the_curr_fow = the_total - the_current
"             redraw | echo '?'. @/ . ' match ' . the_curr_fow . ' of ' . the_total
"         endif
"     catch /^Vim[^)]\+):E486\D/
"         return
"     endtry
" endfunction

" shamelessly stolen from https://github.com/junegunn/vim-slash/
function! smarthl#blinker(times, delay)
  let s:blink = { 'ticks': 2 * a:times, 'delay': a:delay }

  function! s:blink.tick(_)
    let self.ticks -= 1
    let active = self == s:blink && self.ticks > 0

    if !self.clear() && active && &hlsearch
      let [line, col] = [line('.'), col('.')]
      let w:blink_id = matchadd('Cursor',
            \ printf('\%%%dl\%%>%dc\%%<%dc', line, max([0, col-2]), col+2))
    endif
    if active
      call timer_start(self.delay, self.tick)
    endif
  endfunction

  function! s:blink.clear()
    if exists('w:blink_id')
      call matchdelete(w:blink_id)
      unlet w:blink_id
      return 1
    endif
  endfunction

  call s:blink.clear()
  call s:blink.tick(0)
  return ''
endfunction
